# .profile

# Get the aliases and functions
if [ -f ~/.shrc ]; then
        . ~/.shrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin

export PATH

if [ -r /etc/profile.d/workdir.sh ]; then
    .  /etc/profile.d/workdir.sh
fi

