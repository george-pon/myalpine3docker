FROM alpine:3.9

ENV MYALPINEDOCKER_VERSION build-target
ENV MYALPINEDOCKER_VERSION latest
ENV MYALPINEDOCKER_VERSION stable
ENV MYALPINEDOCKER_IMAGE registry.gitlab.com/george-pon/myalpine3docker


# install tools
#  emacs is where?
RUN apk update && apk --update --no-cache add \
    ansible \
    ansible-doc \
    bash \
    bash-doc \
    bind-doc \
    bind-tools \
    curl \
    curl-doc \
    git \
    git-doc \
    jq \
    jq-doc \
    less \
    less-doc \
    man \
    man-pages \
    netcat-openbsd \
    netcat-openbsd-doc \
    openssl \
    openssl-doc \
    rsync \
    rsync-doc \
    sudo \
    sudo-doc \
    tcpdump \
    tcpdump-doc \
    unzip \
    unzip-doc \
    vim \
    vim-doc \
    zip \
    zip-doc

# install docker client
ENV DOCKER_CLIENT_VERSION 18.09.6
RUN curl -fLO https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_CLIENT_VERSION}.tgz && \
    tar xvzf docker-${DOCKER_CLIENT_VERSION}.tgz && \
    cp docker/docker /usr/bin/docker && \
    chmod +x /usr/bin/docker && \
    rm -rf docker

# install docker from comminity apk site
#RUN echo "http://dl-cdn.alpinelinux.org/alpine/latest-stable/community" >>  /etc/apk/repositories && \
#    apk update && \
#    apk --update --no-cache add docker docker-doc

# install kubectl CLI
ENV KUBERNETES_CLIENT_VERSION v1.14.1
RUN curl -fLO https://storage.googleapis.com/kubernetes-release/release/${KUBERNETES_CLIENT_VERSION}/bin/linux/amd64/kubectl && \
    mv  kubectl  /usr/bin/kubectl && \
    chmod +x /usr/bin/kubectl

# install helm CLI v2.9.1
ENV HELM_CLIENT_VERSION v2.9.1
RUN curl -fLO https://storage.googleapis.com/kubernetes-helm/helm-${HELM_CLIENT_VERSION}-linux-amd64.tar.gz && \
    tar xzf  helm-${HELM_CLIENT_VERSION}-linux-amd64.tar.gz && \
    /bin/cp  linux-amd64/helm   /usr/bin && \
    /bin/rm -rf rm helm-${HELM_CLIENT_VERSION}-linux-amd64.tar.gz linux-amd64

# install kompose v1.18.0
ENV KOMPOSE_VERSION v1.18.0
RUN curl -fLO https://github.com/kubernetes/kompose/releases/download/${KOMPOSE_VERSION}/kompose-linux-amd64.tar.gz && \
    tar xzf kompose-linux-amd64.tar.gz && \
    chmod +x kompose-linux-amd64 && \
    mv kompose-linux-amd64 /usr/bin/kompose && \
    rm kompose-linux-amd64.tar.gz

# install stern
ENV STERN_VERSION 1.10.0
RUN curl -fLO https://github.com/wercker/stern/releases/download/${STERN_VERSION}/stern_linux_amd64 && \
    chmod +x stern_linux_amd64 && \
    mv stern_linux_amd64 /usr/bin/stern

# install kustomize
ENV KUSTOMIZE_VERSION 1.0.11
RUN curl -fLO https://github.com/kubernetes-sigs/kustomize/releases/download/v${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64 && \
    chmod +x kustomize_${KUSTOMIZE_VERSION}_linux_amd64 && \
    mv kustomize_${KUSTOMIZE_VERSION}_linux_amd64 /usr/bin/kustomize

# install kubectx, kubens. see https://github.com/ahmetb/kubectx
RUN curl -fLO https://raw.githubusercontent.com/ahmetb/kubectx/master/kubectx && \
    curl -fLO https://raw.githubusercontent.com/ahmetb/kubectx/master/kubens && \
    chmod +x kubectx kubens && \
    mv kubectx kubens /usr/local/bin

# install kubeval ( validate Kubernetes yaml file to Kube-API )
ENV KUBEVAL_VERSION 0.7.3
RUN curl -fLO https://github.com/garethr/kubeval/releases/download/$KUBEVAL_VERSION/kubeval-linux-amd64.tar.gz && \
    tar xf kubeval-linux-amd64.tar.gz && \
    cp kubeval /usr/local/bin && \
    /bin/rm kubeval-linux-amd64.tar.gz

# install kubetest ( lint kubernetes yaml file )
ENV KUBETEST_VERSION 0.1.1
RUN curl -fLO https://github.com/garethr/kubetest/releases/download/$KUBETEST_VERSION/kubetest-linux-amd64.tar.gz && \
    tar xf kubetest-linux-amd64.tar.gz && \
    cp kubetest /usr/local/bin && \
    /bin/rm kubetest-linux-amd64.tar.gz

# install yamlsort see https://github.com/george-pon/yamlsort
ENV YAMLSORT_VERSION v0.1.19
RUN curl -fLO https://github.com/george-pon/yamlsort/releases/download/${YAMLSORT_VERSION}/linux_amd64_yamlsort_${YAMLSORT_VERSION}.tar.gz && \
    tar xzf linux_amd64_yamlsort_${YAMLSORT_VERSION}.tar.gz && \
    chmod +x linux_amd64_yamlsort && \
    mv linux_amd64_yamlsort /usr/bin/yamlsort && \
    rm linux_amd64_yamlsort_${YAMLSORT_VERSION}.tar.gz

ADD docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENV HOME /root

RUN mkdir -p     $HOME
ADD shrc         $HOME/.shrc
ADD profile      $HOME/.profile
ADD bashrc       $HOME/.bashrc
ADD bash_profile $HOME/.bash_profile
ADD vimrc        $HOME/.vimrc
ADD emacsrc      $HOME/.emacs
ENV ENV $HOME/.bashrc

ADD bin /usr/local/bin
RUN chmod +x /usr/local/bin/*.sh

# add sudo user
# https://qiita.com/iganari/items/1d590e358a029a1776d6 Dockerコンテナ内にsudoユーザを追加する - Qiita
# ユーザー名 alpine
# パスワード hogehoge
RUN addgroup -g 1000 alpine && \
    ( echo 'hogehoge'; echo 'hogehoge' ; echo 'hogehoge' ; sleep 3 ) | adduser -S -h /home/alpine -G alpine -s /bin/bash alpine && \
    echo 'alpine:hogehoge' | chpasswd && \
    echo 'Defaults visiblepw'            >> /etc/sudoers && \
    echo 'alpine ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# use normal user alpine
# USER alpine

CMD ["/usr/local/bin/docker-entrypoint.sh"]

